import { Outlet } from 'react-router-dom'

import { useState } from 'react'
import Header from './components/Header'

import Footer from './components/Footer'

import './style.scss'

function Layout() {
  const card = JSON.parse(localStorage.getItem('card')) || []
  const favorites = JSON.parse(localStorage.getItem('favorites')) || []

  const [cartCounter, setCartCounter] = useState(card)
  const [favoritesCounter, setFavoritesCounter] = useState(favorites)

  return (
    <>
      <Header cartCounter={cartCounter} favoritesCounter={favoritesCounter} />

      <Outlet
        context={{
          setCartCounter,
          cartCounter,
          favoritesCounter,
          setFavoritesCounter,
        }}
      ></Outlet>

      <Footer />
    </>
  )
}

export default Layout
