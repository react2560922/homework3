import { createBrowserRouter } from 'react-router-dom'
import Layout from './Layout'
import Card from './components/Card'
import Products from './components/ProductsCatrAndFav'

export const Router = createBrowserRouter([
  {
    path: '/',
    element: <Layout />,
    children: [
      {
        index: true,
        element: <Card />,
      },
      {
        path: '/cart',
        element: <Products id="1" />,
      },
      {
        path: '/favorites',
        element: <Products id="2" />,
      },
    ],
  },
])
