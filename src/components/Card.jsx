import './Card.scss'
import { useState, useEffect } from 'react'
import ProductsItem from './ProductsItem'

function Card() {
  const [products, setProducts] = useState([])

  useEffect(() => {
    fetch('./products.json')
      .then((response) => response.json())
      .then((data) => setProducts(data))
      .catch((error) => console.error('Error fetching products:', error))
  }, [])

  return (
    <>
      <ProductsItem products={products} />
    </>
  )
}

export default Card
