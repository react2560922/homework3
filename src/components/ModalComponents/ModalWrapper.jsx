import './ModalWrapper.scss'

function ModalWrapper({ children, onClick }) {
  return (
    <div className="modal__wrapper2" onClick={onClick}>
      {children}
    </div>
  )
}
export default ModalWrapper
