import Modal from './Modal'
import Header from './ModalComponents/ModalHeader'
import Footer from './ModalComponents/ModalFooter'
import Body from './ModalComponents/ModalBody'
import { useOutletContext } from 'react-router-dom'

import Wrapper from './ModalComponents/ModalWrapper'

import './ModalComponents/ModalFooter.scss'
import ModalClose from './ModalComponents/ModalClose'

function ModalImage({ modal, setModal, nameProduct, product }) {
  const { setCartCounter } = useOutletContext()

  function handleSecondaryClic(productItem) {
    const updatedCard = product.filter((item) => item.sku !== productItem.sku)
    console.log(updatedCard)
    localStorage.setItem('card', JSON.stringify(updatedCard))
    setCartCounter(updatedCard)
    setModal(!modal)
  }

  function handleFirstClick() {
    setModal(!modal)
  }

  return modal ? (
    <Wrapper
      onClick={(event) => {
        if (event.target.classList.contains('modal__wrapper')) {
          setModal(!modal)
        }
      }}
    >
      <Modal>
        <Header>
          <ModalClose
            className="modal__close"
            onClick={() => {
              setModal(!modal)
            }}
          />
        </Header>

        <Body>
          <img src={nameProduct.imageUrl} className="modal__body-img" alt="" />
          <h2 className="modal__body-title">{nameProduct.name} Delete!</h2>
          <p className="modal__body-text">
            By clicking the “Yes, Delete” button, <b>{nameProduct.name} </b>
            will be deleted.
          </p>
        </Body>
        <Footer
          firstText="NO, CANCEL"
          firstClick={handleFirstClick}
          secondaryText="YES, DELETE"
          secondaryClick={() => handleSecondaryClic(nameProduct)}
        />
      </Modal>
    </Wrapper>
  ) : null
}

export default ModalImage
