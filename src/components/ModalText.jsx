import Modal from './Modal'
import Header from './ModalComponents/ModalHeader'
import Footer from './ModalComponents/ModalFooter'
import Body from './ModalComponents/ModalBody'
import Close from './ModalComponents/ModalClose'
import Wrapper from './ModalComponents/ModalWrapper'
import Button from './Button'

function ModalText({ modal, setModal, nameProduct }) {
  return modal ? (
    <Wrapper
      onClick={(event) => {
        if (event.target.classList.contains('modal__wrapper2')) {
          setModal(!modal)
        }
      }}
    >
      <Modal>
        <Header>
          <Close
            onClick={() => {
              setModal(!modal)
            }}
          />
        </Header>

        <Body>
          {/* <h2 className="modal__body-title">“{nameProduct}”</h2> */}
          <p className="modal__body-text">Товар був доданий до кошику</p>
        </Body>
        {/* <Footer>
          <Button className="modal__footer-btn footer-btn2 btn">
            ADD TO FAVORITE
          </Button>
        </Footer> */}
      </Modal>
    </Wrapper>
  ) : null
}

export default ModalText
