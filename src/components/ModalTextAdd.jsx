import Modal from './Modal'

import Body from './ModalComponents/ModalBody'

import Wrapper from './ModalComponents/ModalWrapper'

function ModalTextAdd({ modal, nameProduct }) {
  return modal ? (
    <Wrapper>
      <Modal>
        <Body>
          <p className="modal__body-text-add">Товар був доданий до кошику</p>
        </Body>
      </Modal>
    </Wrapper>
  ) : null
}

export default ModalTextAdd
