import '../style.scss'
import ModalClose from './ModalComponents/ModalClose'
import { useOutletContext } from 'react-router-dom'
import { useState } from 'react'
import ModalImage from './ModalImage'

function ProductsCatrAndFav({ id }) {
  console.log(id)
  let product = []
  let keyLocal = ''

  const { setFavoritesCounter, cartCounter, favoritesCounter } =
    useOutletContext()
  const [modalProduct, setModalProduct] = useState(null)

  const [modal, setModal] = useState(false)

  if (+id === 1) {
    product = cartCounter
    keyLocal = 'card'
  }
  if (+id === 2) {
    product = favoritesCounter
    keyLocal = 'favorites'
  }

  function deleteFavorites(productItem, product) {
    const updatedFavorites = product.filter(
      (item) => item.sku !== productItem.sku
    )

    localStorage.setItem('favorites', JSON.stringify(updatedFavorites))
    setFavoritesCounter(updatedFavorites)
  }

  return (
    <>
      <div className="bestsellers-content conteiner">
        {product.length === 0 ? (
          <p className="modal__body-text">Товар відсутній</p>
        ) : (
          <ul className="bestsellers-content__list">
            {modal ? (
              <ModalImage
                nameProduct={modalProduct}
                modal={modal}
                setModal={setModal}
                product={product}
              />
            ) : null}
            {product.map((productItem) => (
              <li key={productItem.sku} className="bestsellers-content__item">
                <ModalClose
                  id={id}
                  className="modal__close-item"
                  onClick={() => {
                    keyLocal === 'card' ? setModal(!modal) : null
                    setModalProduct(productItem)
                    keyLocal === 'favorites'
                      ? deleteFavorites(productItem, product)
                      : null
                  }}
                />
                <img
                  src={productItem.imageUrl}
                  alt=""
                  className="bestsellers-content__link-img"
                />
                <p className="bestsellers-content__link-name">
                  {productItem.name}
                </p>

                <div className="bestsellers-content__price">
                  <p className="price-bestsellers">{productItem.price} €</p>
                </div>
              </li>
            ))}
          </ul>
        )}
      </div>
    </>
  )
}

export default ProductsCatrAndFav
