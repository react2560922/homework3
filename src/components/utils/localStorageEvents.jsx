import '../Card.scss'

export const addToCart = (
  setModal,
  setIsActive,
  productItem,
  cartCounter,
  setCartCounter
) => {
  setModal(true)

  const isExist = cartCounter.some((obj) => obj.sku === productItem.sku)

  setIsActive((prevState) => ({ ...prevState, [productItem.sku]: true }))

  const lokalCard = JSON.parse(localStorage.getItem('card')) || []

  if (!isExist) {
    setCartCounter((prev) => [...prev, productItem])
    lokalCard.push(productItem)
    localStorage.setItem('card', JSON.stringify(lokalCard))
  }
}

export const addToFavorites = (
  setIsActiveFav,
  productItem,

  favoritesCounter,
  setFavoritesCounter
) => {
  const isExist = favoritesCounter.some((obj) => obj.sku === productItem.sku)

  setIsActiveFav((prevState) => ({ ...prevState, [productItem.sku]: true }))

  const lokalFavorites = JSON.parse(localStorage.getItem('favorites')) || []

  if (!isExist) {
    setFavoritesCounter((prev) => [...prev, productItem])
    lokalFavorites.push(productItem)
    localStorage.setItem('favorites', JSON.stringify(lokalFavorites))
  }
}
